# Matrix Solver 

from scratch Matrix Solver  using Python3 

## What is Matrix /?
![alt text](http://static2.mbtfiles.co.uk/media/docs/newdocs/international_baccalaureate/maths/877857/html/images/image24.png)
_In mathematics, a matrix is a rectangular ' of numbers, symbols, or expressions, arranged in s and s. For example, the dimension of the matrix below is 2 × 3, because there are two rows and three columns._

##What is a cofactor?
![alt text](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.lq0htJRLR8274LHFDnE5YAHaCE%26pid%3DApi&f=1)
A cofactor is a number that is obtained by eliminating the row and column of a particular element which is in the form of a square or rectangle. The cofactor is preceded by a negative or positive sign based on the element’s position.

## what is Matrix Inverse
![alt text](https://cdn.kastatic.org/ka-youtube-converted/ArcrdMkEmKo.mp4/ArcrdMkEmKo.png)

If A is a non-singular square matrix, there is an existence of n x n matrix A-1, which is called the inverse matrix of A such that it satisfies the property:
AA-1 = A-1A = I, where I is  the Identity matrix

## what is Determinants of Matrix
![alt text](https://i.pinimg.com/736x/b2/f1/8a/b2f18ad26042cabfa7a4aa15d62e752c.jpg)
Determinants are widely used in various fields like in Engineering, Economics, Science, Social Science and many more. Determinant of a Matrix must be computed with its scalar value, for every given square matrix. The square matrices are of 2x2 matrix, 3x3 matrix or nxn matrices. Matrix is represented as get A or det (A) pr |A|. The number of columns in a matrix is equal to the number of rows.

## Solving With Python3.
![alt text](https://xp.io/storage/1AJtMeDL.png)
![alt text](https://xp.io/storage/1AJCeL1N.png)

## Thanks 
