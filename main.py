import numpy as np  
import math as m 
import time 
import os 

os.system('clear')

print("Want To Try Your Matrix Data Press Y or y\n Else press any key")
g=input(">")
if g == "y"or g == "Y":
    print("start putting Data Line Wise")
    print("")
    user = []
    for i in range(9):
        try:
            row= float(input(f'{i+1}>>> '))
            user.append(row)
        except:
            print('Wrong Input')
            row= float(input(f'{i+1}>>> '))
            user.append(row)
    print('')
    a =np.array([[user[0],user[1],user[2]],[user[3],user[4],user[5]],[user[6],user[7],user[8]]])
else:
    a = np.array([[5,-2,1],[3,1,-4],[6,0,-3]])
    print('')



def print_mat(a):
    print('Given Matrix','\n')
    print('A = ')
    return np.array(a)
def Shape(a):
    print('')
    return 'Given Matrix shape '+str(np.shape(a))
def Dim(a):
    print('')
    return a.ndim
def Determinants_first_value_given(a):
    
    First = a[0][0]*(a[1][1]*a[2][-1]-a[1][-1]*a[-1][1])
    pr_nt1= f'{a[0][0]}{a[1][1]} x {a[2][-1]} - {a[1][-1]} x {a[-1][1]}'
    return  First#,pr_nt1
def Solving_fun(a):
    pr_nt1= f'{a[0][0]}(({a[1][1]} x {a[2][-1]}) - ({a[1][-1]} x {a[-1][1]}))'
    pr_nt2 = f'-{a[0][1]}(({a[1][0]}) x ({a[-1][-1]}) - ({a[1][-1]}) x ({a[-1][0]}))'
    pr_nt3 = f'{a[0][-1]} (({a[1][0]})x({a[-1][1]})-({a[1][1]}) x ({a[-1][0]}))'
    return pr_nt1+""+pr_nt2+''+pr_nt3
    
def Determinants(a):
    return  np.linalg.det(a)

def Matrix_Inverse(a):
    return np.linalg.inv(a)

def Print_Invers(a):
    check_Invers_is_not_0= Determinants(a)
    if check_Invers_is_not_0 >0 or check_Invers_is_not_0<0:
        print('|A| not 0 So Inverse Existing')
        print('')
        print(Matrix_Inverse(a))
    else:
        print('|A| is = 0 ')
    return ''

def Minors(a):
    Minors_data_dic={
    'M11' : a[1][1]*a[2][-1]-a[1][-1]*a[-1][1],
    'M12' : a[1][0]*a[-1][-1]-a[1][-1]*a[-1][0],
    'M13' : a[1][0]*a[-1][1]-a[1][1]*a[-1][0],
    'M21' : a[0][1]*a[-1][-1]-a[0][-1]*a[-1][1],
    'M22' : a[0][0]*a[-1][-1]-a[0][-1]*a[-1][0],
    'M23' : a[0][0]*a[-1][1]-a[0][1]*a[-1][-1],
    'M31' : a[0][1]*a[1][-1]-a[0][-1]*a[1][1],
    'M32' : a[0][0]*a[1][-1]-a[0][-1]*a[1][0],
    'M33' : a[0][0]*a[1][1]-a[0][1]*a[1][0]
    }
    return Minors_data_dic
def Print_minoar(a):
    S(1)
    Minors_data_dic2= Minors(a)
    print('Minors_data = ','\nM11 = '+str(Minors_data_dic2['M11']),'M12 = '+str(Minors_data_dic2['M12']),'M13 = '+str(Minors_data_dic2['M13']),'\nM21 = '+str(Minors_data_dic2['M21']),'M22 = '+str(Minors_data_dic2['M22']),'M23 = '+str(Minors_data_dic2['M23']),'\nM31 = '+str(Minors_data_dic2['M31']),'M32 = '+str(Minors_data_dic2['M32']),'M33 = '+str(Minors_data_dic2['M33']))
    return ''

def Cofactor(a):
    Minors_data_for_Calculation= Minors(a)
    Cofactor_data_dic={
    'c11' : m.pow((-1),1+1)*Minors_data_for_Calculation['M11'],
    'c12' : m.pow((-1),1+2)*Minors_data_for_Calculation['M12'],
    'c13' : m.pow((-1),1+3)*Minors_data_for_Calculation['M13'],
    'c21' : m.pow((-1),2+1)*Minors_data_for_Calculation['M21'],
    'c22' : m.pow((-1),2+2)*Minors_data_for_Calculation['M22'],
    'c23' : m.pow((-1),2+3)*Minors_data_for_Calculation['M23'],
    'c31' : m.pow((-1),3+1)*Minors_data_for_Calculation['M31'], 
    'c32' : m.pow((-1),3+2)*Minors_data_for_Calculation['M32'], 
    'c33' : m.pow((-1),3+3)*Minors_data_for_Calculation['M33']}
    return Cofactor_data_dic

def Print_Cofactor(a):
    S(1)
    Cofactor_data_dic2 = Cofactor(a)
    print('Cofactors_data =','\nc11 = '+str(Cofactor_data_dic2['c11']),'c12 = '+str(Cofactor_data_dic2['c12']),'c13 = '+str(Cofactor_data_dic2['c13']),'\nc21 = '+str(Cofactor_data_dic2['c21']),'c22 = '+str(Cofactor_data_dic2['c22']),'c23 = '+str(Cofactor_data_dic2['c23']),'\nc31 = '+str(Cofactor_data_dic2['c31']),'c32 = ' +str(Cofactor_data_dic2['c32']),'c33 = '+str(Cofactor_data_dic2['c33']))
    return ''


def Adjoint(a):
    Cofactor_data_convert_Adjoint= Cofactor(a)
    print('Adjoint = ','\n',np.array([[Cofactor_data_convert_Adjoint['c11'],Cofactor_data_convert_Adjoint['c21'],Cofactor_data_convert_Adjoint['c31']],[Cofactor_data_convert_Adjoint['c12'],Cofactor_data_convert_Adjoint['c22'],Cofactor_data_convert_Adjoint['c32']],[Cofactor_data_convert_Adjoint['c13'],Cofactor_data_convert_Adjoint['c23'],Cofactor_data_convert_Adjoint['c33']]]))
    return ''

def S(n):
    for i in range(n):
        return ''

def Main():
    pass

print(Main())




